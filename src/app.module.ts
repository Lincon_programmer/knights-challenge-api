import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { KnightsModule } from './knights/knights.module';

@Module({
  imports: [
    KnightsModule,
    MongooseModule.forRoot(
      'mongodb+srv://linconsantos:3fnr3hfxpc@cluster-dev.jiwg1k2.mongodb.net/?retryWrites=true&w=majority',
    ),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
