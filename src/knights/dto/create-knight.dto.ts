import { ApiProperty } from '@nestjs/swagger';
import { Attributes, Weapon } from '../interfaces/knight.interface';

export class CreateKnightDto {
  @ApiProperty({ description: 'Nome do cavaleiro' })
  name: string;
  @ApiProperty({ description: 'Apelido do cavaleiro' })
  nickname: string;
  @ApiProperty({ description: 'Data de nascimento do cavaleiro' })
  birthday: string;
  @ApiProperty({ description: 'Arma do cavaleiro' })
  weapons: Weapon[];
  @ApiProperty({ description: 'Atributos do cavaleiro' })
  attributes: Attributes;
  @ApiProperty({ description: 'Atributo chave do cavaleiro' })
  keyAttribute: string;
}
