import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';
import { Attributes, Weapon } from '../interfaces/knight.interface';

export class UpdateKnightDto {
  @ApiPropertyOptional({ description: 'Apelido do Knight' })
  @IsString()
  @IsOptional()
  nickname?: string;

  @ApiPropertyOptional({ description: 'Nome do Knight' })
  @IsString()
  @IsOptional()
  name?: string;

  @ApiPropertyOptional({ description: 'Arma do Knight' })
  @IsOptional()
  weapons?: Weapon[];

  @ApiPropertyOptional({ description: 'Atributos do Knight' })
  @IsOptional()
  attributes?: Attributes;

  @ApiPropertyOptional({ description: 'Atributo chave do Knight' })
  @IsString()
  @IsOptional()
  keyAttribute?: string;
}
