import { ApiProperty } from '@nestjs/swagger';

export class Weapon {
  @ApiProperty({ description: 'Nome da arma' })
  name: string;

  @ApiProperty({ description: 'Mod da arma' })
  mod: number;

  @ApiProperty({ description: 'Atributo relacionado à arma' })
  attr: string;

  @ApiProperty({ description: 'Indica se a arma está equipada' })
  equipped: boolean;
}

export class Attributes {
  @ApiProperty({ description: 'Força' })
  strength: number;

  @ApiProperty({ description: 'Destreza' })
  dexterity: number;

  @ApiProperty({ description: 'Constituição' })
  constitution: number;

  @ApiProperty({ description: 'Inteligência' })
  intelligence: number;

  @ApiProperty({ description: 'Sabedoria' })
  wisdom: number;

  @ApiProperty({ description: 'Carisma' })
  charisma: number;
}

export class Knight {
  @ApiProperty({ description: 'ID do cavaleiro' })
  id?: string;

  @ApiProperty({ description: 'Nome do cavaleiro' })
  name: string;

  @ApiProperty({ description: 'Apelido do cavaleiro' })
  nickname: string;

  @ApiProperty({ description: 'Data de nascimento do cavaleiro (YYYY-MM-DD)' })
  birthday: string;

  @ApiProperty({ description: 'Armas do cavaleiro' })
  weapons: Weapon[];

  @ApiProperty({ description: 'Atributos do cavaleiro' })
  attributes: Attributes;

  @ApiProperty({ description: 'Atributo chave do cavaleiro' })
  keyAttribute: string;

  @ApiProperty({ description: 'Idade do cavaleiro', required: false })
  age?: number;

  @ApiProperty({ description: 'Poder de ataque do cavaleiro', required: false })
  attack?: number;

  @ApiProperty({ description: 'Experiência do cavaleiro', required: false })
  experience?: number;
}
