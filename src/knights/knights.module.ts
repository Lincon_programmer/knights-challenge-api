import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { KnightsController } from './knights.controller';
import { KnightsService } from './knights.service';
import { Knight, KnightSchema } from './schemas/knight.schema';
import { Hero, HeroSchema } from './schemas/hero.schema';
@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Knight.name, schema: KnightSchema },
      { name: Hero.name, schema: HeroSchema },
    ]),
  ],
  controllers: [KnightsController],
  providers: [KnightsService],
})
export class KnightsModule {}
