import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { Attributes, Weapon } from '../interfaces/knight.interface';

@Schema()
export class Knight extends Document {
  @Prop()
  name: string;

  @Prop()
  nickname: string;

  @Prop()
  birthday: string;

  @Prop([{ type: Object }])
  weapons: Weapon[];

  @Prop({ type: Object })
  attributes: Attributes;

  @Prop()
  keyAttribute: string;
}

export const KnightSchema = SchemaFactory.createForClass(Knight);
