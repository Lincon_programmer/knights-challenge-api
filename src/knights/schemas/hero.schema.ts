import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { Attributes, Weapon } from '../interfaces/knight.interface';

export type HeroDocument = Hero & Document;

@Schema()
export class Hero {
  @Prop()
  name: string;

  @Prop()
  nickname: string;

  @Prop()
  birthday: string;

  @Prop([{ type: Object }])
  weapons: Weapon[];

  @Prop({ type: Object })
  attributes: Attributes;

  @Prop()
  keyAttribute: string;
}

export const HeroSchema = SchemaFactory.createForClass(Hero);
