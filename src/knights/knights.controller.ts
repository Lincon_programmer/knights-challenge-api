import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  Put,
  Query,
} from '@nestjs/common';
import { KnightsService } from './knights.service';
import { CreateKnightDto } from './dto/create-knight.dto';
import { UpdateKnightDto } from './dto/update-knight.dto';
import { ApiBody, ApiQuery, ApiTags } from '@nestjs/swagger';

@Controller('knights')
export class KnightsController {
  constructor(private readonly knightsService: KnightsService) {}

  @ApiTags('knights')
  @Get()
  @ApiQuery({
    name: 'filter',
    required: false,
    description: 'Filtro para retornar apenas heróis',
  })
  findAll(@Query('filter') filter?: string) {
    if (filter === 'heroes') {
      return this.knightsService.findAllHeroes();
    }
    return this.knightsService.findAll();
  }

  @ApiTags('knights')
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.knightsService.findOne(id);
  }

  @ApiTags('knights')
  @Post()
  @ApiBody({
    type: CreateKnightDto,
    description: 'Dados do cavaleiro para criação',
  })
  create(@Body() createKnightDto: CreateKnightDto) {
    return this.knightsService.create(createKnightDto);
  }

  @ApiTags('knights')
  @Put(':id')
  update(@Param('id') id: string, @Body() updateKnightDto: UpdateKnightDto) {
    return this.knightsService.update(id, updateKnightDto);
  }

  @ApiTags('knights')
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.knightsService.remove(id);
  }
}
