import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateKnightDto } from './dto/create-knight.dto';
import { UpdateKnightDto } from './dto/update-knight.dto';
import { Knight } from './interfaces/knight.interface';
import { Hero, HeroDocument } from './schemas/hero.schema';

@Injectable()
export class KnightsService {
  constructor(
    @InjectModel('Knight') private knightModel: Model<Knight>,
    @InjectModel(Hero.name) private readonly heroModel: Model<HeroDocument>,
  ) {}

  async findAll(): Promise<Knight[]> {
    const knights = await this.knightModel.find().exec();
    return knights.map((knight) => this.addExtraInfo(knight));
  }

  async findAllHeroes(): Promise<Hero[]> {
    return await this.heroModel.find().exec();
  }

  async findOne(id: string): Promise<Knight> {
    const knight = await this.knightModel.findById(id).exec();
    if (!knight) {
      throw new NotFoundException(`Knight with id ${id} not found`);
    }
    return this.addExtraInfo(knight);
  }

  async create(createKnightDto: CreateKnightDto): Promise<Knight> {
    const knight = new this.knightModel(createKnightDto);
    return this.addExtraInfo(await knight.save());
  }

  async update(id: string, updateKnightDto: UpdateKnightDto): Promise<Knight> {
    const updatedKnight = await this.knightModel
      .findByIdAndUpdate(id, updateKnightDto, { new: true })
      .exec();
    if (!updatedKnight) {
      throw new NotFoundException(`Knight with id ${id} not found`);
    }
    return this.addExtraInfo(updatedKnight);
  }

  async remove(id: string): Promise<void> {
    const knight = await this.knightModel.findById(id).exec();
    if (!knight) {
      throw new NotFoundException(`Knight with id ${id} not found`);
    }
    // Copie todos os atributos necessários do knight para o novo objeto hero.
    const hero = new this.heroModel({
      name: knight.name,
      nickname: knight.nickname,
      birthday: knight.birthday,
      weapons: knight.weapons,
      attributes: knight.attributes,
      keyAttribute: knight.keyAttribute,
    });
    await hero.save(); // Salve o herói no banco de dados.
    await this.knightModel.findByIdAndRemove(id).exec(); // Remova o cavaleiro do banco de dados.
  }

  private calculateAttack(knight: Knight): number {
    const keyAttribute = knight.attributes[knight.keyAttribute];
    const equippedWeapon = knight.weapons.find((weapon) => weapon.equipped);
    const weaponMod = equippedWeapon ? equippedWeapon.mod : 0;

    const attrMod = this.getAttributeModifier(keyAttribute);

    return 10 + attrMod + weaponMod;
  }

  private getAttributeModifier(value: number): number {
    if (value >= 0 && value <= 8) {
      return -2;
    } else if (value >= 9 && value <= 10) {
      return -1;
    } else if (value >= 11 && value <= 12) {
      return 0;
    } else if (value >= 13 && value <= 15) {
      return 1;
    } else if (value >= 16 && value <= 18) {
      return 2;
    } else {
      return 3;
    }
  }

  private calculateAge(birthday: string): number {
    const birthDate = new Date(birthday);
    const now = new Date();
    const ageDiff = now.getTime() - birthDate.getTime();
    return Math.floor(ageDiff / (1000 * 60 * 60 * 24 * 365.25));
  }

  private addExtraInfo = (knight: Knight): Knight => {
    const age = this.calculateAge(knight.birthday);
    const attack = this.calculateAttack(knight);
    const experience = this.calculateExp(age);

    return {
      ...knight,
      age,
      attack,
      experience,
    };
  };

  private calculateExp(age: number): number {
    if (age < 7) {
      return 0;
    }
    return Math.floor((age - 7) * Math.pow(22, 1.45));
  }
}
